<?php

namespace AppBundle\Security\Core\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseClass
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();
        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';
        $setter_refreshToken = $setter.'RefreshToken';
        $setter_expires = $setter.'Expires';
        $setter_expires_at = $setter.'ExpiresAt';
        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $previousUser->$setter_refreshToken(null);
            $previousUser->$setter_expires(null);
            $this->userManager->updateUser($previousUser);
        }
        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $user->$setter_refreshToken($response->getRefreshToken());
        $user->$setter_expires($response->getExpiresIn());
        $expires = new \DateTime();
        $expires->add(new \DateInterval('PT'.$response->getExpiresIn().'S'));
        $user->$setter_expires_at($expires);
        $this->userManager->updateUser($user);
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        //when the user is registrating
        if (null === $user) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';
            $setter_refreshToken = $setter.'RefreshToken';
            $setter_expires = $setter.'Expires';
            $setter_expires_at = $setter.'ExpiresAt';
            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());
            $user->$setter_refreshToken($response->getRefreshToken());
            $user->$setter_expires($response->getExpiresIn());
            $expires = new \DateTime();
            $expires->add(new \DateInterval('PT'.$response->getExpiresIn().'S'));
            $user->$setter_expires_at($expires);
            //I have set all requested data with the user's username
            //modify here with relevant data
            $user->setUsername($response->getEmail());
            $user->setEmail($response->getEmail());
            $user->setPassword($username);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
            return $user;
        }
        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName);
        $setter_token = $setter.'AccessToken';
        $setter_refreshToken = $setter.'RefreshToken';
        $setter_expires = $setter.'Expires';
        $setter_expires_at = $setter.'ExpiresAt';
        //update access token
        $user->$setter_token($response->getAccessToken());
        $user->$setter_refreshToken($response->getRefreshToken());
        $user->$setter_expires($response->getExpiresIn());
        $expires = new \DateTime();
        $expires->add(new \DateInterval('PT'.$response->getExpiresIn().'S'));
        $user->$setter_expires_at($expires);
        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->userManager->refreshUser($user);
    }
}