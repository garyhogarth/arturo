<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    protected $facebook_id;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    protected $facebook_access_token;

    /**
     * @ORM\Column(name="facebook_refresh_token", type="string", length=255, nullable=true)
     */
    protected $facebook_refresh_token;

    /**
     * @ORM\Column(name="facebook_expires", type="string", length=255, nullable=true)
     */
    protected $facebook_expires;

    /**
     * @ORM\Column(name="facebook_expires_at", type="datetime", nullable=true)
     */
    protected $facebook_expires_at;

    /**
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    protected $google_id;

    /**
     * @ORM\Column(name="locale", type="string", length=10, nullable=true)
     */
    protected $locale;

    /**
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    protected $google_access_token;

    /**
     * @ORM\Column(name="google_refresh_token", type="string", length=255, nullable=true)
     */
    protected $google_refresh_token;

    /**
     * @ORM\Column(name="google_expires", type="string", length=255, nullable=true)
     */
    protected $google_expires;

    /**
     * @ORM\Column(name="google_expires_at", type="datetime", nullable=true)
     */
    private $google_expires_at;


    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    public function __construct()
    {
        parent::__construct();
        $this->setLocale('en');
        // your own logic
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebook_access_token
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set google_id
     *
     * @param string $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get google_id
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set google_access_token
     *
     * @param string $googleAccessToken
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get google_access_token
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getFacebookRefreshToken()
    {
        return $this->facebook_refresh_token;
    }

    /**
     * @param mixed $facebook_refresh_token
     */
    public function setFacebookRefreshToken($facebook_refresh_token)
    {
        $this->facebook_refresh_token = $facebook_refresh_token;
    }

    /**
     * @return mixed
     */
    public function getFacebookExpires()
    {
        return $this->facebook_expires;
    }

    /**
     * @param mixed $facebook_expires
     */
    public function setFacebookExpires($facebook_expires)
    {
        $this->facebook_expires = $facebook_expires;
    }

    /**
     * @return mixed
     */
    public function getGoogleRefreshToken()
    {
        return $this->google_refresh_token;
    }

    /**
     * @param mixed $google_refresh_token
     */
    public function setGoogleRefreshToken($google_refresh_token)
    {
        $this->google_refresh_token = $google_refresh_token;
    }

    /**
     * @return mixed
     */
    public function getGoogleExpires()
    {
        return $this->google_expires;
    }

    /**
     * @param mixed $google_expires
     */
    public function setGoogleExpires($google_expires)
    {
        $this->google_expires = $google_expires;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getGoogleExpiresAt()
    {
        return $this->google_expires_at;
    }

    /**
     * @param mixed $google_expires_at
     */
    public function setGoogleExpiresAt($google_expires_at)
    {
        $this->google_expires_at = $google_expires_at;
    }

    /**
     * @return mixed
     */
    public function getFacebookExpiresAt()
    {
        return $this->facebook_expires_at;
    }

    /**
     * @param mixed $facebook_expires_at
     */
    public function setFacebookExpiresAt($facebook_expires_at)
    {
        $this->facebook_expires_at = $facebook_expires_at;
    }




}