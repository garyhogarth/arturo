<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Buzz\Browser;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\GoogleResourceOwner;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserOAuthListener implements EventSubscriberInterface
{
    /**
     * @var TokenStorage
     */
    private $user;
    private $buzz;
    private $googleId;
    private $googleSecret;

    public function __construct(TokenStorageInterface $tokenStorage, Browser $buzz, $em, $googleId, $googleSecret)
    {
        $this->user = $tokenStorage;
        $this->em = $em;
        $this->buzz = $buzz;
        $this->googleId = $googleId;
        $this->googleSecret = $googleSecret;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();

        if ($this->user->getToken() && $this->user->getToken()->getUser() instanceof User) {
            $user = $this->user->getToken()->getUser();
            if ($user->getGoogleExpiresAt() < new \DateTime()) {
                $response = $this->buzz->post( "https://accounts.google.com/o/oauth2/token", array(),
                    "refresh_token=".$user->getGoogleRefreshToken()."&client_id={$this->googleId}&client_secret={$this->googleSecret}&grant_type=refresh_token"
                );
                if ($response->getContent()) {
                    $responseData = json_decode($response->getContent());
                    $user->setGoogleAccessToken($responseData->access_token);
                    $user->setGoogleExpires($responseData->expires_in);
                    $user->setGoogleExpiresAt($responseData->expires_in);
                    $expires = new \DateTime();
                    $expires->add(new \DateInterval('PT'.$responseData->expires_in.'S'));
                    $user->setGoogleExpiresAt($expires);

                    $this->em->flush();
                }
            }
        }


    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::CONTROLLER => array(array('onKernelController', 17)),
        );
    }
}