<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        $menu->addChild('menu.main.home', array('route' => 'homepage'));

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $menu->addChild('menu.main.logout', array('route' => 'fos_user_security_logout'));
        } else {
            $menu->addChild('menu.main.register_login', array('route' => 'hwi_oauth_service_redirect', 'routeParameters' => ['service' => 'google']));
        }

        return $menu;
    }
}